from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager


class AccountManager(BaseUserManager):
    def create_user(self, username, first_name, last_name, email, password=None):
        if not username:
            raise ValueError("Please fill the username field")
        if not first_name:
            raise ValueError("Please fill it with your First Name")
        if not last_name:
            raise ValueError("I need to know your Last Name, please fill it too :)")
        if not email:
            raise ValueError("I think an email address is important to fill")

        user = self.model(
                username=username,
                first_name=first_name,
                last_name=last_name,
                email=self.normalize_email(email)
            )
        
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, username, first_name, last_name, email, password=None):
        user = self.create_user(
            username=username,
            first_name=first_name,
            last_name=last_name,
            email=self.normalize_email(email),
            password=password
        )

        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class Account(AbstractBaseUser):
    username        = models.CharField(verbose_name="username", max_length=50, unique=True)
    first_name      = models.CharField(verbose_name="nama depan", max_length=100)
    last_name       = models.CharField(verbose_name="nama belakang", max_length=100)
    email           = models.EmailField(verbose_name="email", max_length=100, unique=True)
    date_joined     = models.DateTimeField(verbose_name="date joined", auto_now_add=True)
    last_login      = models.DateTimeField(verbose_name="last login", auto_now=True)
    is_admin        = models.BooleanField(default=False)
    is_active       = models.BooleanField(default=True)
    is_staff        = models.BooleanField(default=False)
    is_superuser    = models.BooleanField(default=False)

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = ["email", "first_name", "last_name"]

    objects = AccountManager()

    def __str__(self):
        return self.username

    def has_perm(self, perm, obj=None):
        return self.is_admin

    def has_module_perms(self, app_label):
        return True
