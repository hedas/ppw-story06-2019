from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .models import Account


class AccountAdmin(UserAdmin):
    list_display = ['username', 'email', 'first_name', 'last_name', 'last_login', 'is_staff', 'is_admin']
    search_fields = ['username', 'email', 'first_name', 'last_name']
    readonly_fields = ['last_login', 'date_joined']

    filter_horizontal = []
    list_filter = []
    fieldsets = []

admin.site.register(Account, AccountAdmin)
