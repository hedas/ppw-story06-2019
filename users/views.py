from django.shortcuts import render, redirect
from django.contrib.auth import login, logout, authenticate
from .forms import RegistrationForm, AccountAuthenticationForm

def registration_view(request):
    content = {}
    if request.user.is_authenticated:
        return redirect('landing-page')

    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            plain_password = form.cleaned_data.get('password1')
            account = authenticate(username=username, password=plain_password)
            login(request, account)
            request.session[username] = account.first_name
            return redirect('landing-page')
        else:
            content["registration_form"] = form
    else:
        form = RegistrationForm()
        content["registration_form"] = form

    return render(request, 'users/register.html', content)

def logout_view(request):
    del request.session[request.user.username]
    logout(request)
    return redirect("landing-page")

def login_view(request):
    content = {}
    user = request.user
    if request.user.is_authenticated:
        return redirect('landing-page')

    if request.method == 'POST':
        form = AccountAuthenticationForm(request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(username=username, password=password)
            if user :
                login(request, user)
                request.session[username] = user.first_name
                return redirect('landing-page')
        

    else:
        form = AccountAuthenticationForm()

    content['login_form'] = form

    return render(request, 'users/login.html', content)