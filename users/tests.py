from django.test import TestCase, Client
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.auth import login, logout
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
import time
from users.models import AccountManager, Account

class AccountTest(TestCase):

    @classmethod
    def setUpClass(cls):
        # super(TwitterLiteTest, cls).setUpClass()
        super().setUpClass()
        cls.user = Account.objects.create_user("UjiCoba", "Uji", "Coba", "uji@coba.com")
        cls.user.set_password("passwordujicoba123")
        cls.user.save()



    def test_index(self):
        index = Client().get('/user/login/')
        self.assertEqual(index.status_code, 200)

    def test_login_button(self):
        index = Client().get("/")
        self.assertIn("Sign In", index.content.decode())
    
    def test_register_button(self):
        index = Client().get("/")
        self.assertIn("Sign Up", index.content.decode())

    def test_account_object(self):
        user = Account.objects.get(username="UjiCoba")
        self.assertEqual(user.first_name, 'Uji')
        self.assertEqual(user.last_name, 'Coba')

    # def test_greet_account(self):
    #     # user = Account.objects.get(username="UjiCoba")
    #     # self.client.force_login(self.user)
    #     c = Client()
    #     logged_in = c.login(username='UjiCoba', password="passwordujicoba123")
    #     index = Client().get("/")
    #     self.assertIn("Hello, UjiCoba", index.content.decode())

    # def test_button_logout(self):
    #     # user = Account.objects.get(username="UjiCoba")
    #     # self.client.force_login(self.user)
    #     c = Client()
    #     logged_in = c.login(username='UjiCoba', password="passwordujicoba123")
    #     index = Client().get("/")
    #     self.assertIn("Logout", index.content.decode())

    def test_register_form(self):
        index = Client().get("/user/register/")
        self.assertIn("<form", index.content.decode())
    
    def test_login_form(self):
        index = Client().get("/user/login/")
        self.assertIn("<form", index.content.decode())






class AccountFunctionalTest(StaticLiveServerTestCase):
    # port = 8000

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = Account.objects.create_user("UjiCoba", "Uji", "Coba", "uji@coba.com", "passwordujicoba123")
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        # chrome_options.setBinary('/usr/local/bin/')
        cls.browser = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
        cls.browser.get(cls.live_server_url)


    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super().tearDownClass()

    def test_login(self):
        self.browser.get(self.live_server_url + "/user/login/")
        username_input = self.browser.find_element_by_id("id_username")
        password_input = self.browser.find_element_by_id("id_password")
        username_input.send_keys('UjiCoba')
        password_input.send_keys('passwordujicoba123')
        password_input.send_keys("\n")


        self.assertIn("Hello, ", self.browser.page_source)
        self.browser.get(self.live_server_url + "/")
        self.assertIn("Logout", self.browser.page_source)

    def test_logout(self):
        self.browser.get(self.live_server_url + "/user/login/")
        username_input = self.browser.find_element_by_id("id_username")
        password_input = self.browser.find_element_by_id("id_password")
        username_input.send_keys('UjiCoba')
        password_input.send_keys('passwordujicoba123')
        password_input.send_keys("\n")
        self.browser.get(self.live_server_url + "/user/logout/")

        self.browser.get(self.live_server_url)
        self.assertIn("Sign In", self.browser.page_source)
        self.assertIn("Sign Up", self.browser.page_source)

