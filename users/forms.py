from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate
from .models import Account


class RegistrationForm(UserCreationForm):
    username = forms.CharField(max_length=50, help_text="This username will be your login id", widget=forms.TextInput(attrs={'placeholder': 'Username', 'class': "form-control" }))
    first_name = forms.CharField(label="First Name", max_length=100, widget=forms.TextInput(attrs={'placeholder': 'First Name', 'class': "form-control" }))
    last_name = forms.CharField(label="Last Name", max_length=100, widget=forms.TextInput(attrs={'placeholder': 'Last Name', 'class': "form-control" }))
    email = forms.EmailField(max_length=100, help_text="Add a valid email address", widget=forms.EmailInput(attrs={'placeholder': 'Email', 'class': "form-control" }))
    password1 = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={'placeholder': 'Password', 'class': "form-control" }))
    password2 = forms.CharField(label="Password Confirmation", widget=forms.PasswordInput(attrs={'placeholder': 'Password', 'class': "form-control" }))


    class Meta:
        model = Account
        fields = ["username", 'first_name', 'last_name', 'email', 'password1', 'password2']

class AccountAuthenticationForm(forms.ModelForm):
    username = forms.CharField(label="Username", widget=forms.TextInput(attrs={'placeholder': 'Username', 'class': "form-control" }))
    password = forms.CharField(label="Password", widget=forms.PasswordInput(attrs={'placeholder': 'Password', 'class': "form-control" }))

    class Meta:
        model = Account
        fields = ['username', 'password']

    def clean(self):
        username = self.cleaned_data['username']
        password = self.cleaned_data['password']
        user = authenticate(username=username, password=password)
        if not user:
            raise forms.ValidationError("Invalid username/password when login")