from django.contrib import admin
from django.urls import path, include
from . import views

app_name = "users"

urlpatterns = [
    path("register/", views.registration_view, name='register'),
    path("logout/", views.logout_view, name='logout'),
    path("login/", views.login_view, name='login')
]