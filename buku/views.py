from django.shortcuts import render
from django.http import HttpResponse
from requests import get
from json import loads
import random

# Create your views here.
def buku_view(request):
    url = "https://www.googleapis.com/books/v1/volumes?q="
    keyword = random.choice(list("abcdefghijklmnopqrstuvwxz"))
    result = get(url + keyword).text
    result = loads(result)
    return render(request, "buku/buku.html", {"books" : result["items"]})

# def cari_buku(request):
#     url = "https://www.googleapis.com/books/v1/volumes?q="
#     print(request.GET)
#     if "search" in request.GET :
#         keyword = request.GET['search']
#         print("masuk")
#     else:
#         keyword = random.choice(list("abcdefghijklmnopqrstuvwxz"))

#     print("KEYWORDNYA ADALAH " + keyword)
#     result = get(url + keyword).text
#     print(url + keyword)
#     # print(result)
#     result = loads(result)
#     return HttpResponse(content=result, content_type="application/json")