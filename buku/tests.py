from django.test import TestCase, Client
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
import time

# Create your tests here.
class Library(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()


class TwitterLiteFunctionalTest(StaticLiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        cls.browser = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
        cls.browser.get(cls.live_server_url)


    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super().tearDownClass()