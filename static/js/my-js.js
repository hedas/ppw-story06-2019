$(document).ready(function(){
    $(".card .kartu-badan").hide();
    $(".card .kartu-badan").first().show();
    
    $(".card-header").click(function() {
        if($(this).siblings().is(":hidden")) {
            $(this).siblings().slideDown(800);
            if($(this).parent().siblings().children(".kartu-badan").is(":visible")) {
                $(this).parent().siblings().children(".kartu-badan").slideUp(800);    
            }
        }
    });
    
    if (typeof(Storage) !== "undefined") {
        if (localStorage.checked) {
            localStorage.checked = localStorage.checked;
        } else {
            localStorage.checked = true;
        }
    }
    
    
    $('.switch > input').click(function() {
        if((localStorage.checked ==  "true")){
            // $('.switch > input').prop('checked', false);
            localStorage.checked = false;

            $('.outer').first().css("background-color", "rgb(255, 255, 243)")
            $('.outer').last().css("background-color", "rgb(23, 162, 184)")
            $('#all-status').css("background-color", "rgb(23, 162, 184)")
            $('.outer > .inner >#greet').css("color", "rgb(0, 0, 0)")

            $('.navbar').css('background-color', 'rgb(255, 255, 243)');
            $("#navbar").attr('class', 'navbar navbar-expand-md navbar-light border-bottom');
            $('.navbar-brand').attr('style', 'color: black !important');
            $(".nav-item").attr('style' ,'color: black !important')

            $('.profile-page').css("background-color", "rgb(255, 255, 243)");
            $('.profile-heading').attr('style', 'color: black !important');
            $(".profile-page > .row > .col-md-6 > .card > .card-header").css('background-color', 'rgb(23, 162, 184)');
            $(".kartu-badan").css('background-color', 'white');

            $('#search').css("background-color", "rgb(255, 255, 243)");
            $('#demo').css("background-color", "rgb(255, 255, 243)");
            $('.text-library').attr('style', 'color: black !important');

            
        }else {
            // $('.switch > input').prop('checked', true);
            localStorage.checked = true;

            $('.outer').first().css("background-color", "rgb(0, 0, 12)")
            $('.outer').last().css("background-color", "rgb(85, 192, 156)")
            $('#all-status').css("background-color", "rgb(85, 192, 156)")
            $('.outer > .inner >#greet').css("color", "rgb(207, 207, 207)")

            $('.navbar').css("background-color", "rgb(0, 0, 12)");
            $("#navbar").attr('class', 'navbar navbar-expand-md navbar-dark border-bottom');
            $('.navbar-brand').attr('style', 'color: white !important');
            $('.nav-item').attr('style', 'color: white !important')

            $('.profile-page').css("background-color", "rgb(0, 0, 12)");
            $('.profile-heading').attr('style', 'color: white !important');
            $(".profile-page > .row > .col-md-6 > .card > .card-header").css('background-color', 'rgb(85, 192, 156)');
            $(".kartu-badan").css('background-color', 'papayawhip');

            $('#search').css("background-color", "rgb(0, 0, 12)")
            $('#demo').css("background-color", "rgb(0, 0, 12)")
            $('.text-library').attr('style', 'color: white !important');

        }
    });
    if((localStorage.checked ==  "true")){
        // $('.switch > input').prop('checked', false);
        $('.switch > input').attr('checked', true);
    }else {
        // $('.switch > input').prop('checked', true);
        $('.switch > input').attr('checked', false);
    }
    
    if(!(localStorage.checked == "true")){
        // $('.switch > input').prop('checked', false);
        localStorage.checked = false;

        $('.outer').first().css("background-color", "rgb(255, 255, 243)")
        $('.outer').last().css("background-color", "rgb(23, 162, 184)")
        $('#all-status').css("background-color", "rgb(23, 162, 184)")
        $('.outer > .inner >#greet').css("color", "rgb(0, 0, 0)")

        $('.navbar').css('background-color', 'rgb(255, 255, 243)');
        $("#navbar").attr('class', 'navbar navbar-expand-md navbar-light border-bottom');
        $('.navbar-brand').attr('style', 'color: black !important');
        $(".nav-item").attr('style' ,'color: black !important')

        $('.profile-page').css("background-color", "rgb(255, 255, 243)");
        $('.profile-heading').attr('style', 'color: black !important');
        $(".profile-page > .row > .col-md-6 > .card > .card-header").css('background-color', 'rgb(23, 162, 184)');
        $(".profile-page > .row > .col-md-6 > .card > .kartu-badan").css('background-color', 'white');

        $('#search').css("background-color", "rgb(255, 255, 243)");
        $('#demo').css("background-color", "rgb(255, 255, 243)");
        $('.text-library').attr('style', 'color: black !important');
    }else {
        // $('.switch > input').prop('checked', true);
        localStorage.checked = true;

        $('.outer').first().css("background-color", "rgb(0, 0, 12)")
        $('.outer').last().css("background-color", "rgb(85, 192, 156)")
        $('#all-status').css("background-color", "rgb(85, 192, 156)")
        $('.outer > .inner >#greet').css("color", "rgb(207, 207, 207)")

        $('.navbar').css("background-color", "rgb(0, 0, 12)");
        $("#navbar").attr('class', 'navbar navbar-expand-md navbar-dark border-bottom');
        $('.navbar-brand').attr('style', 'color: white !important');
        $('.nav-item').attr('style', 'color: white !important')

        $('.profile-page').css("background-color", "rgb(0, 0, 12)");
        $('.profile-heading').attr('style', 'color: white !important');
        $(".profile-page > .row > .col-md-6 > .card > .card-header").css('background-color', 'rgb(85, 192, 156)');
        $(".profile-page > .row > .col-md-6 > .card > .kartu-badan").css('background-color', 'papayawhip');

        $('#search').css("background-color", "rgb(0, 0, 12)")
        $('#demo').css("background-color", "rgb(0, 0, 12)")
        $('.text-library').attr('style', 'color: white !important');
    }
});

