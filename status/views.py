from django.shortcuts import render, redirect
from .forms import StatusForm
from .models import Status
import random
from requests import get
from json import loads



def UpdateStatus(request):
    # try:
    #     print(request.session[request.user.username])
    # except Exception as e:
    #     print('anonim')

    meow = get('https://uselessfacts.jsph.pl/random.json').text
    meow = loads(meow)['text']
    PLACEHOLDER = [
        "Kuy lah gibah...",
        "Eh dia kan...",
        "Ada yang tau dia kenapa???",
        "Tadi liat ga...",
        "HEHEHEHEHEH...."
    ]
    i = random.choice(PLACEHOLDER)
    data = Status.objects.all().order_by('-date_time')
    if request.method == "POST":
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/#message-status')
    else:
        form = StatusForm()

    return render(request, 'status/landing_page.html', {'form' : form, 'data' : data, 'PLACEHOLDER' : meow})

def Profile(request):
    return render(request, "status/profile.html")