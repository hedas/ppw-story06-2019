from django import forms
from .models import Status
import random

# i = random.randint(0, 5)

# PLACEHOLDER = [
#     "Kuy lah gibah...",
#     "Eh dia kan...",
#     "Ada yang tau dia kenapa???",
#     "Tadi liat ga...",
#     "HEHEHEHEHEH...."
# ]


class StatusForm(forms.ModelForm):
    class Meta:
        PLACEHOLDER = [
            "Kuy lah gibah...",
            "Eh dia kan...",
            "Ada yang tau dia kenapa???",
            "Tadi liat ga...",
            "HEHEHEHEHEH...."
        ]
        model = Status
        fields = ['message']
        i = random.randint(0, 4)
        # widgets = {
        #     'message': forms.Textarea(
        #         attrs={'placeholder' : PLACEHOLDER[i]}),
        # }
