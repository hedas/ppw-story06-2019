from django.test import TestCase, Client
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
import time
from status.models import Status

class TwitterLiteTest(TestCase):

    @classmethod
    def setUpClass(cls):
        # super(TwitterLiteTest, cls).setUpClass()
        super().setUpClass()
        cls.hai = Status.objects.create(message="Hai")
        cls.hello = Status.objects.create(message="Hello")

    def test_index(self):
        index = Client().get('/')
        self.assertEqual(index.status_code, 200)

    def test_status_object(self):
        hai = Status.objects.get(message="Hai")
        hello = Status.objects.get(message="Hello")
        self.assertEqual(hai.message, 'Hai')
        self.assertEqual(hello.message, 'Hello')

    def test_greet_in_template(self):
        message = 'Halo, apa kabar?'
        response = Client().get('/')
        self.assertIn(message, response.content.decode())

    def test_form(self):
        response = Client().get('/')
        form_tag = '<form'
        self.assertIn(form_tag, response.content.decode())

    def test_button_update(self):
        response = Client().get('/')
        id_button = 'id="UpdateButton"'
        self.assertIn(id_button, response.content.decode())
    
    def test_return(self):
        str(self.hai)

    def test_sent_post(self):
        response = Client().post(path='/', data={'message': "Sean"}, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_profile_name(self):
        response = Client().get('/profile/')
        profile_name = "Sean Zeliq Urian"
        self.assertIn(profile_name, response.content.decode())

    def test_photo_profile(self):
        response = Client().get('/profile/')
        image = 'id="profile-image"'
        self.assertIn(image, response.content.decode())




class TwitterLiteFunctionalTest(StaticLiveServerTestCase):
    # port = 8000

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        # chrome_options.setBinary('/usr/local/bin/')
        cls.browser = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
        cls.browser.get(cls.live_server_url)


    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super().tearDownClass()

    def test_post(self):
        self.browser.get(self.live_server_url)
        status = self.browser.find_element_by_id('id_message')
        submit = self.browser.find_element_by_id('UpdateButton')
        status.send_keys('Coba Coba')
        submit.send_keys("\n")
        # submit.click()

        self.browser.get(self.live_server_url)
        self.assertIn('Coba Coba', self.browser.page_source)      
        # self.tearDown()

    def test_tittle(self):
        self.browser.get(self.live_server_url)
        self.assertEqual(self.browser.title, 'Home - TwitterLite')
        # self.tearDown()

    def test_greet(self):
        self.browser.get(self.live_server_url)
        greet = self.browser.find_element_by_id('greet')
        self.assertIn(greet.text, self.browser.page_source)
        # self.tearDown()
        
    def test_theme(self):
        self.browser.get(self.live_server_url)
        button = self.browser.find_elements_by_class_name('switch')
        button[0].click()
        self.assertEqual(self.browser.find_element_by_id('navbar').value_of_css_property('background-color'),'rgba(255, 255, 243, 1)' )

    def test_acordion(self):
        self.browser.get(self.live_server_url + "/profile/")

        acordion_head = self.browser.find_elements_by_class_name('kartu-kepala')
        acordion_body = self.browser.find_elements_by_class_name('kartu-badan')

        for a, b in zip(acordion_head, acordion_body):
            a.location_once_scrolled_into_view
        
            a.click()
            self.assertIn(b.text, self.browser.page_source)
        

    
