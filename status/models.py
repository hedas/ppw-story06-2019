from django.db import models

class Status(models.Model):
    message = models.TextField(max_length=300)
    date_time = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "{} {}".format(self.date_time, self.message)
